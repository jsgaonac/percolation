#!/bin/bash

for i in 5 10 20 50 70 100 300;
do
	echo "Running with n = " $i ";" "Saving in ../resultados/datos/datos"$i".csv"
	./percolation $i ../resultados/datos/datos$i.csv
done
