// Copyright (c) 2015 - Juan Sebastian Gaona C. - jsgaonac@unal.edu.co 

// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


#include "../TinyMT/tinymt/tinymt64.h"
#include "../list/src/list.h"

#include <stdio.h>
#include <stdlib.h>


#define SUCCESS 0
#define FAILURE 1

#define HOLLOW 0
#define SOLID 1

#define EQUAL 0
#define NOT_EQ 1

#define N_TESTS 2500

typedef struct
{
	int x;
	int y;
} Cell;


int allocate_grid(size_t n, char*** grid);
int percolates(list_t* visited_cells, size_t n, char** grid);

void do_percolation(size_t n, char** grid, const char* filename);
void initialize_grid(size_t n, char** grid, double p, tinymt64_t* random);

int compare_cells(Cell* one, Cell* two);
void set_cell(Cell* c, char x, char y);

int find_cell(list_t* list, int x, int y);
void add_cell(list_t* visited_cells, int x, int y);
void cleanup_list(list_t* list);
void print_grid(size_t n, char** grid);

int main(int argc, char const *argv[])
{
	if (argc < 3)
	{
		printf("%s\n", "El programa requiere 2 argumentos:");
		printf("%s\n", "(1) Tamaño del grid [0, 1000], (2) Nombre del archivo de resultados");
		printf("%s\n", "Ejemplo: percolation 50 resultados50.csv");

		return 1;
	}

	size_t n = atoi(argv[1]);

	if (n > 1000)
	{
		printf("%s\n", "Ingrese un valor menor o igual a 1000.");
		return 1;
	}

	if (n > 100)
	{
		printf("%s\n", "Con valores superiores a 100 el programa puede tomar un tiempo considerable.");
	}

	char** grid = NULL;

	if (allocate_grid(n, &grid) == FAILURE)
	{
		printf("%s\n", "Hubo un error al asignar la memoria del grid.");
		return 1;
	}

	do_percolation(n, grid, argv[2]);	

	return 0;
}

int allocate_grid(size_t n, char*** grid)
{
	*grid = (char**) malloc(sizeof(char*) * n);

	if (*grid == NULL)
	{
		return FAILURE;
	}

	for (size_t i = 0; i < n; i++)
	{
		(*grid)[i] = (char*) malloc(n);

		if ((*grid)[i] == NULL)
		{
			for (size_t j = 0; j < i; j++)
			{
				free((*grid)[j]);
			}

			free(*grid);

			return FAILURE;
		}
	}

	return SUCCESS;
}

void do_percolation(size_t n, char** grid, const char* filename)
{
	FILE* file = fopen(filename, "w");

	if (file == NULL)
	{
		printf("%s%s%s\n", "No se pudo crear el archivo: ", filename);
		exit(1);
	}

	tinymt64_t random;

	for (double p = 0; p <= 1.01; p += 0.01)
	{
		int percolations = 0;

		for (int i = 0; i < N_TESTS; i++)
		{
			initialize_grid(n, grid, p, &random);

			list_t* visited_cells = list_new();

			for (size_t j = 0; j < n; j++)
			{
				if (grid[0][j] == HOLLOW)
				{
					if (find_cell(visited_cells, j, 0) == FAILURE)
					{
						add_cell(visited_cells, j, 0);

						if (percolates(visited_cells, n, grid) == SUCCESS)
						{
							percolations++;

							break;
						}
					}
				}
			}

			cleanup_list(visited_cells);
		}

		double theta_p = (double)percolations / N_TESTS;

		fprintf(file, "%2.2f; %.3f\n", p, theta_p);
	}

	fclose(file);

	/* Cleanup */
	for (size_t i = 0; i < n; i++)
	{
		free(grid[i]);
	}

	free(grid);
}

void initialize_grid(size_t n, char** grid, double p, tinymt64_t* random)
{
	for (size_t i = 0; i < n; i++)
	{
		for (size_t j = 0; j < n; j++)
		{
			grid[i][j] = tinymt64_generate_double01(random) < p ? SOLID : HOLLOW;
		}
	}
}

int compare_cells(Cell* one, Cell* two)
{
	if (one->x == two->x && one->y == two->y) return EQUAL;

	return NOT_EQ;
}

void set_cell(Cell* c, char x, char y)
{
	c->x = x;
	c->y = y;
}

int percolates(list_t* visited_cells, size_t n, char** grid)
{
	Cell* last_c = (Cell*)visited_cells->tail->val;

	int x = last_c->x;
	int y = last_c->y;

	if (y == n - 1)
	{
		return SUCCESS;
	}

	if (y + 1 < n)
	{
		if (grid[x][y + 1] == HOLLOW)
		{
			if (find_cell(visited_cells, x, y + 1) == FAILURE)
			{
				add_cell(visited_cells, x, y + 1);

				if (percolates(visited_cells, n, grid) == SUCCESS)
				{
					return SUCCESS;
				}
			}
		}
	}

	if (x + 1 < n)
	{
		if (grid[x + 1][y] == HOLLOW)
		{
			if (find_cell(visited_cells, x + 1, y) == FAILURE)
			{
				add_cell(visited_cells, x + 1, y);

				if (percolates(visited_cells, n, grid) == SUCCESS)
				{
					return SUCCESS;
				}
			}
		}
	}

	if (x - 1 >= 0)
	{
		if (grid[x - 1][y] == HOLLOW)
		{
			if (find_cell(visited_cells, x - 1, y) == FAILURE)
			{
				add_cell(visited_cells, x - 1, y);

				if (percolates(visited_cells, n, grid) == SUCCESS)
				{
					return SUCCESS;
				}
			}
		}		
	}

	if (y - 1 >= 0)
	{
		if (grid[x][y - 1] == HOLLOW)
		{
			if (find_cell(visited_cells, x, y - 1) == FAILURE)
			{
				add_cell(visited_cells, x, y - 1);

				if (percolates(visited_cells, n, grid) == SUCCESS)
				{
					return SUCCESS;
				}
			}		
		}
	}

	return FAILURE;
}

int find_cell(list_t* list, int x, int y)
{
	Cell c;
	c.x = x;
	c.y = y;

	list_node_t *node = NULL;
	list_iterator_t *iter = list_iterator_new(list, LIST_HEAD);

	while ((node = list_iterator_next(iter)))
	{
		Cell* v = (Cell*) node->val;

		if (compare_cells(&c, v) == EQUAL)
		{
			list_iterator_destroy(iter);
			return SUCCESS;
		}
	}

	list_iterator_destroy(iter);

	return FAILURE;
}

void add_cell(list_t* visited_cells, int x, int y)
{
	Cell* c = (Cell*)malloc(sizeof(Cell));
	c->x = x;
	c->y = y;

	list_node_t* c_node = list_node_new(c);
	list_rpush(visited_cells, c_node);
}

void cleanup_list(list_t* list)
{
	list_node_t *node = NULL;
	list_iterator_t *iter = list_iterator_new(list, LIST_HEAD);

	while ((node = list_iterator_next(iter)))
	{
		if (node->val != NULL)
		{
			free(node->val);
			node->val = NULL;
		}
	}

	list_iterator_destroy(iter);

	list_destroy(list);
}

void print_grid(size_t n, char** grid)
{
	printf("\n");
	for (size_t i = 0; i < n; i++)
	{
		for (size_t j = 0; j < n; j++)
		{
			printf("%d ", grid[i][j]);
		}

		printf("\n");
	}
}